# TESIS: SPARQL APP REAL TIME#

Esta aplicación me permite consultar datos de la mayoría de los países del mundo desde la Web Semántica.

**PROYECTO CON SPARQL**: *APP REAL TIME PARA MI PROYECTO DE TESIS USANDO SPARQL, AJAX Y ADMIN LTE*

**Autor:** Jorge Luis Martínez.

**SPARQL** es un acrónimo recursivo del inglés SPARQL Protocol and RDF Query Language. Se trata de un lenguaje estandarizado para la consulta de grafos **RDF**, normalizado por el RDF Data Access Working Group (DAWG) del World Wide Web Consortium (W3C). Es una tecnología clave en el desarrollo de la Web Semántica que se constituyó como Recomendación oficial del W3C el 15 de enero de 2008.

Documentation LTE ADMIN
-----------------------
Visit the [online documentation](https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html) for the most
updated guide. Information will be added on a weekly basis.

License
-------
AdminLTE is an open source project by [Almsaeed Studio](https://almsaeedstudio.com) that is licensed under [MIT](http://opensource.org/licenses/MIT). Almsaeed Studio
reserves the right to change the license of future releases.