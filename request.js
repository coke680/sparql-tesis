function query(){

  var querySouthAmerica = "prefix dbpedia: <http://dbpedia.org/resource/>" +
                    "    prefix dbpedia-owl: <http://dbpedia.org/ontology/>"+
                    "    PREFIX yago: <http://dbpedia.org/class/yago/>"+

                    "    SELECT DISTINCT ?pais ?nombre ?thumbnail "+
                    "    WHERE {"+

                    "    ?pais rdf:type yago:SouthAmericanCountries . "+
                    "    ?pais foaf:name ?nombre .  filter (?nombre != 'font-size:88%;'@en) . "+

                    "    ?pais dbpedia-owl:thumbnail ?thumbnail . } "+

                    "    ORDER BY ASC(?nombre) OFFSET 1"

  var queryEurope = "    prefix dbpedia: <http://dbpedia.org/resource/>" +
                    "    prefix dbpedia-owl: <http://dbpedia.org/ontology/>"+
                    "    PREFIX yago: <http://dbpedia.org/class/yago/>"+

                    "    SELECT DISTINCT ?pais ?nombre ?thumbnail"+
                    "    WHERE {"+

                    "    ?pais rdf:type yago:EuropeanCountries ."+
                    "    ?pais rdfs:label ?nombre .  "  +
                    "    filter (?nombre != 'font-size:88%;'@en) .  "  +
                    "    filter (?nombre != 'font-size:80%;'@en) . "+
                    "    filter (?nombre != 'Ελληνική Δημοκρατία'@en) . "+
                    "    filter (?nombre != 'Україна'@en) . "+
                    "    filter (?nombre != 'Црна Гора'@en) "+
                    "    ?pais dbpedia-owl:thumbnail ?thumbnail .  FILTER (LANG(?nombre) = 'en')}"+

                    "    ORDER BY ASC(?nombre) "

  var queryAfrica = "    prefix dbpedia: <http://dbpedia.org/resource/>" +
                    "    prefix dbpedia-owl: <http://dbpedia.org/ontology/>"+
                    "    PREFIX yago: <http://dbpedia.org/class/yago/>"+

                    "    SELECT DISTINCT ?pais ?nombre ?thumbnail"+
                    "    WHERE {"+

                    "    ?pais rdf:type yago:AfricanCountries ."+
                    "    ?pais rdfs:label ?nombre .  "  +
                    "    filter (?nombre != 'font-size:88%;'@en) .  "  +
                    "    filter (?nombre != 'font-size:80%;'@en) . "+
                    "    filter (?nombre != 'Ελληνική Δημοκρατία'@en) . "+
                    "    filter (?nombre != 'Україна'@en) . "+
                    "    filter (?nombre != 'Црна Гора'@en) "+
                    "    ?pais dbpedia-owl:thumbnail ?thumbnail .  FILTER (LANG(?nombre) = 'en')}"+

                    "    ORDER BY ASC(?nombre) "


  var queryOceania = "   prefix dbpedia: <http://dbpedia.org/resource/>" +
                    "    prefix dbpedia-owl: <http://dbpedia.org/ontology/>"+
                    "    PREFIX yago: <http://dbpedia.org/class/yago/>"+

                    "    SELECT DISTINCT ?pais ?nombre ?thumbnail"+
                    "    WHERE {"+

                    "    ?pais rdf:type yago:OceanianCountries ."+
                    "    ?pais rdfs:label ?nombre .  "  +
                    "    filter (?nombre != 'font-size:88%;'@en) .  "  +
                    "    filter (?nombre != 'font-size:80%;'@en) . "+
                    "    filter (?nombre != 'Ελληνική Δημοκρατία'@en) . "+
                    "    filter (?nombre != 'Україна'@en) . "+
                    "    filter (?nombre != 'Црна Гора'@en) "+
                    "    ?pais dbpedia-owl:thumbnail ?thumbnail .  FILTER (LANG(?nombre) = 'en')}"+

                    "    ORDER BY ASC(?nombre) "

  var queryAmericaCentral = "   prefix dbpedia: <http://dbpedia.org/resource/>" +
                    "    prefix dbpedia-owl: <http://dbpedia.org/ontology/>"+
                    "    PREFIX yago: <http://dbpedia.org/class/yago/>"+

                    "    SELECT DISTINCT ?pais ?nombre ?thumbnail"+
                    "    WHERE {"+

                    "    ?pais rdf:type yago:CentralAmericanCountries ."+
                    "    ?pais rdfs:label ?nombre .  "  +
                    "    filter (?nombre != 'font-size:88%;'@en) .  "  +
                    "    filter (?nombre != 'font-size:80%;'@en) . "+
                    "    filter (?nombre != 'Ελληνική Δημοκρατία'@en) . "+
                    "    filter (?nombre != 'Україна'@en) . "+
                    "    filter (?nombre != 'Црна Гора'@en) "+
                    "    ?pais dbpedia-owl:thumbnail ?thumbnail .  FILTER (LANG(?nombre) = 'en')}"+

                    "    ORDER BY ASC(?nombre) "

    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {
                query: querySouthAmerica,
                format: 'application/sparql-results+json',
              },
      url: 'http://dbpedia.org/sparql',
      success: function(data){
            var array = data.results.bindings;
            //resultados
            for(var i=0; i < array.length; i++){
                var item = array[i];
                //Se extraen los valores
                var pais = item.pais.value;
                var nombre = item.nombre.value;
                var thumbnail = item.thumbnail.value;

                $( "#menu-country" ).append("<li><a href='#' class='pais'><i class='fa fa-flag'></i><span>"+nombre+"</span></a></li>");
            }

        },

        error: function (obj, error, objError){
            alert('No fue posible extraer la información de SUDAMÉRICA.');
        }
    });

    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {
                query: queryEurope,
                format: 'application/sparql-results+json',
              },
      url: 'http://dbpedia.org/sparql',
      success: function(data){
            var array = data.results.bindings;
            //resultados
            for(var i = 0; i < array.length; i++){
                var item = array[i];
                //Se extraen los valores
                var pais = item.pais.value;
                var nombre = item.nombre.value;
                var thumbnail = item.thumbnail.value;

                $( "#menu-country-europa" ).append("<li><a href='#' class='pais'><i class='fa fa-flag'></i><span>"+nombre+"</span></a></li>");

            }

        },

        error: function (obj, error, objError){
            alert('No fue posible extraer la información de EUROPA.');
        }
    });

    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {
                query: queryAfrica,
                format: 'application/sparql-results+json',
              },
      url: 'http://dbpedia.org/sparql',
      success: function(data){
          var array = data.results.bindings;
          //resultados
          for(var i = 0; i < array.length; i++){
              var item = array[i];
              //Se extraen los valores
              var pais = item.pais.value;
              var nombre = item.nombre.value;
              var thumbnail = item.thumbnail.value;

              $( "#menu-country-africa" ).append("<li><a href='#' class='pais'><i class='fa fa-flag'></i><span>"+nombre+"</span></a></li>");

          }

      },

      error: function (obj, error, objError){
          alert('No fue posible extraer la información de AFRICA.');
      }
  });

    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {
                query: queryOceania,
                format: 'application/sparql-results+json',
              },
      url: 'http://dbpedia.org/sparql',
      success: function(data){
          var array = data.results.bindings;
          //resultados
          for(var i = 0; i < array.length; i++){
              var item = array[i];
              //Se extraen los valores
              var pais = item.pais.value;
              var nombre = item.nombre.value;
              var thumbnail = item.thumbnail.value;

              $( "#menu-country-oceania" ).append("<li><a href='#' class='pais'><i class='fa fa-flag'></i><span>"+nombre+"</span></a></li>");

          }

      },

      error: function (obj, error, objError){
          alert('No fue posible extraer la información de Oceanía.');
      }
  });

    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {
                query: queryAmericaCentral,
                format: 'application/sparql-results+json',
              },
      url: 'http://dbpedia.org/sparql',
      success: function(data){
          var array = data.results.bindings;
          //resultados
          for(var i = 0; i < array.length; i++){
              var item = array[i];
              //Se extraen los valores
              var pais = item.pais.value;
              var nombre = item.nombre.value;
              var thumbnail = item.thumbnail.value;

              $( "#menu-country-acentral" ).append("<li><a href='#' class='pais'><i class='fa fa-flag'></i><span>"+nombre+"</span></a></li>");

          }

      },

      error: function (obj, error, objError){
          alert('No fue posible extraer la información de América central.');
      }
  });


}

$(document).ready(function() {
    $('#countries').on('click', '.pais', function(event) {
        event.preventDefault();

        var pais = $(this).text();
        var NombrePais = pais.replace(" ", "_");
        ajaxReload(NombrePais);

        $('#content-main').removeClass('hide');
        $('#welcome').addClass('hide')
    });

});

function ajaxReload(NombrePais){

  var query = " PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> " +
              " PREFIX dbpedia: <http://dbpedia.org/resource/> " +
              " PREFIX prop: <http://dbpedia.org/property/> "+

              " SELECT DISTINCT ?nombre ?capital ?AreaTotal ?presidente ?Moneda ?NombreMoneda ?thumbnail ?resumen ?NombreCapital ?population ?latitud ?longitud ?resumenCapital ?thumbnailCapital ?NombrePresidente" +
              " WHERE " +
              " { " +
              " dbpedia:"+NombrePais+" rdfs:label ?nombre; dbpedia-owl:capital ?capital ; dbp:areaKm ?AreaTotal; dbpedia-owl:leader ?presidente; dbpedia-owl:currency ?Moneda; dbpedia-owl:thumbnail ?thumbnail ; " +
              " dbpedia-owl:abstract ?resumen FILTER ((LANG(?resumen) = 'es') && (?nombre != 'font-size:88%;'@en) && (?nombre != 'font-size:80%;'@en) && (?nombre != 'font-size:78%;'@en)) ." +
              "    filter (?nombre != 'Ελληνική Δημοκρατία'@en) . "+
              "    filter (?nombre != 'Україна'@en) . "+
              "    filter (?nombre != 'Црна Гора'@en) . "+
              " ?capital rdfs:label ?NombreCapital FILTER (LANG(?NombreCapital) = 'es') .  FILTER (LANG(?nombre) = 'es') . dbpedia:"+NombrePais+" dbp:populationEstimate ?population . " +
              " ?capital geo:lat ?latitud . ?capital geo:long ?longitud ." +
              " ?capital dbpedia-owl:abstract ?resumenCapital  . FILTER (LANG(?resumenCapital) = 'es') . " +
              " ?capital dbpedia-owl:thumbnail ?thumbnailCapital . " +
              " OPTIONAL { ?presidente foaf:name ?NombrePresidente } . OPTIONAL { ?Moneda rdfs:label ?NombreMoneda .  FILTER (LANG(?NombreMoneda) = 'es')} } LIMIT 1 "
              
     $.ajax({

        type: 'GET',
        dataType: 'jsonp',
        data: {
                query: query,
                format: 'application/sparql-results+json',
              },
        url: 'http://dbpedia.org/sparql',

        success: function(data){
              var array = data.results.bindings;
              //resultados
              for(var i = 0; i < array.length; i++){
                  var item = array[i];
                  //Se extraen los valores
                  var nombre = item.nombre.value;
                  var thumbnail = item.thumbnail.value;
                  var resumen = item.resumen.value;
                  var NombreMoneda = item.NombreMoneda.value;
                  var NombreCapital = item.NombreCapital.value;
                  var AreaTotal = item.AreaTotal.value;
                  var population = item.population.value;
                  var latitud = item.latitud.value;
                  var longitud = item.longitud.value;
                  var resumenCapital = item.resumenCapital.value;
                  var thumbnailCapital = item.thumbnailCapital.value;
                  var NombrePresidente = item.NombrePresidente.value
                
                  $( "#info-pais" ).append("<p>"+resumen+"</p>");
                  $( ".nombre" ).append("<h1>"+nombre+"</h1>");
                  $(".capital").append("<h5 class='widget-user-desc'><strong>Capital:</strong> "+NombreCapital+"</h5>");
                  $( ".presidente" ).append("<h5><strong>Presidente:</strong> "+NombrePresidente+"</h5>");
                  $( ".foto" ).append("<div class='widget-user-image'>\
                                          <img class='img-responsive' src="+thumbnail+">\
                                        </div>");
                  $( ".foto-capital" ).append("<img class='img-responsive thumbnail' src="+thumbnailCapital+">");

                  $( ".population" ).append("<div class='small-box bg-green'>\
                                                <div class='inner'><h3>"+population+"</h3>\
                                                  <p>Número de habitantes</p>\
                                                </div>\
                                                <div class='icon'>\
                                                  <i class='ion ion-person'></i>\
                                                </div>\
                                                <a target='_blank' href='http://dbpedia.org/page/"+nombre+"' class='small-box-footer'>Más información <i class='fa fa-arrow-circle-right'></i></a>\
                                              </div>");
                  $( ".area" ).append("<div class='small-box bg-red'>\
                                                <div class='inner'><h3>"+AreaTotal+"</h3>\
                                                  <p>Superficie terrestre (KM cuadrados)</p>\
                                                </div>\
                                                <div class='icon'>\
                                                  <i class='ion ion-android-walk'></i>\
                                                </div>\
                                                <a target='_blank' href='http://dbpedia.org/page/"+nombre+"' class='small-box-footer'>Más información <i class='fa fa-arrow-circle-right'></i></a>\
                                              </div>");
                  $( ".datos" ).append("<div class='small-box bg-yellow'>\
                                                <div class='inner'><h3>"+NombreMoneda+"</h3>\
                                                  <p>Moneda nacional</p>\
                                                </div>\
                                                <div class='icon'>\
                                                  <i class='ion ion-cash'></i>\
                                                </div>\
                                                <a target='_blank' href='http://dbpedia.org/page/"+nombre+"' class='small-box-footer'>Más información <i class='fa fa-arrow-circle-right'></i></a>\
                                              </div>");

                  //GOOGLE MAPS MARKERS 

                  var myLatLng = new google.maps.LatLng(parseFloat(latitud),parseFloat(longitud));

                  var map = new google.maps.Map(document.getElementById('map'), {
                       zoom: 10,
                       center: myLatLng
                  });            

                  var contentString = '<div id="content">'+
                                          '<div id="siteNotice">'+
                                          '</div>'+
                                          '<h3 id="firstHeading" class="firstHeading">'+NombreCapital+'</h3>'+
                                          '<div id="bodyContent">'+                                                        
                                              '<p>'+resumenCapital+'</p>'+
                                          '</div>'+
                                      '</div>';

                  var infowindow = new google.maps.InfoWindow({
                      content: contentString
                  });

                  var marker = new google.maps.Marker({
                       position: myLatLng,
                       map: map,
                       title: 'Capital!'
                  });

                  marker.addListener('click', function() {
                      infowindow.open(map, marker);
                  });

              }
          },
           error: function (obj, error, objError){
              alert('No fue posible extraer la información.');
          }
    });
        $("#info-pais").empty();
        $(".nombre").empty();
        $(".capital").empty();
        $(".foto").empty();
        $( ".population" ).empty();
        $( ".foto-capital" ).empty();
        $( ".presidente" ).empty();
        $( ".area" ).empty();
        $( ".datos" ).empty()
}
