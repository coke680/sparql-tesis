//FORMULARIO
$(document).ready(function() {
    $('#search-form').on('click', '#submit', function(event) {
        event.preventDefault();
        var form = $("#text-area").val();
        var query = form.split("\n").join(" ");

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            data: {
                    query: query,
                    format: 'application/sparql-results+json',
                  },
            url: 'http://dbpedia.org/sparql',

            success: function(data){

                var array = data.results.bindings; 

                $.each(array, function(i){
                    
                    var item = array[i];
                    var datos = JSON.stringify(item);
                    alert(datos);
                    
                    $("#contentPane").append(datos + " ");

                });
            },

            error: function (obj, error, objError){
                alert('No fue posible extraer la información consultada. Revise su código en busca de errores.');
            }
        });

       $("#contentPane").empty();
       
    });
});
